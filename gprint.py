'author = gsaito'
from termcolor import colored

messageInfo = "INFO:    ==> "
messageError = "ERROR:   ==> "
messageWarning = "WARNING: ==> "

info = 'info'
warning = 'warning'
error = 'error'


def gprint(string, type=info, id=None):
    """
    The string can be any valid Python string;
    The type can be info, warning or error;
    and id a integer within [1..4]
    """
    if type == 'info':
        print(colored(messageInfo, 'white', attrs=[
              'bold']), end="")
        if id == 1:
            print(colored(string, 'blue'))
        elif id == 2:
            print(colored(string, 'green'))
        elif id == 3:
            print(colored(string, 'magenta'))
        elif id == 4:
            print(colored(string, 'yellow'))
        else:
            print(colored(string, 'white'))
    if type == 'warning':
        print(colored(messageWarning + string,
                      'white', 'on_yellow', attrs=['bold']))
    if type == 'error':
        print(colored(messageError + string,
                      'white', 'on_red', attrs=['bold']))
