from termcolor import colored

def welcome(message=""):       

  print("""
       d8888 88888888888 888             d8888  .d8888b.        888     888  .d8888b.  8888888b.  
      d88888     888     888            d88888 d88P  Y88b       888     888 d88P  Y88b 888   Y88b 
     d88P888     888     888           d88P888 Y88b.            888     888 Y88b.      888    888 
    d88P 888     888     888          d88P 888  "Y888b.         888     888  "Y888b.   888   d88P 
   d88P  888     888     888         d88P  888     "Y88b.       888     888     "Y88b. 8888888P"  
  d88P   888     888     888        d88P   888       "888       888     888       "888 888        
 d8888888888     888     888       d8888888888 Y88b  d88P       Y88b. .d88P Y88b  d88P 888        
d88P     888     888     88888888 d88P     888  "Y8888P"         "Y88888P"   "Y8888P"  888        
  """)
  print("                                                "+message)
  print("")
  print("")
  print("")
  print("")


